import com.android.build.OutputFile
import com.android.build.gradle.internal.api.ApkVariantOutputImpl
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
}

android {
    namespace = "org.proninyaroslav.libretorrent"
    compileSdkVersion(33)
    defaultConfig {
        applicationId = "org.proninyaroslav.libretorrent"
        minSdkVersion(24)
        targetSdkVersion(33)
        versionCode = 25
        /*
         * Convection:
         *  major.minor.[revision][-some_info]
         */
        versionName = "3.4"
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true

        ndk {
            abiFilters.addAll(listOf("armeabi-v7a", "arm64-v8a", "x86", "x86_64"))
        }

        javaCompileOptions {
            annotationProcessorOptions {
                arguments += mapOf("room.schemaLocation" to "$projectDir/schemas".toString())
            }
        }

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    flavorDimensions += "deploy"

    productFlavors {
        create("base") {
            dimension = "deploy"
        }
        create("play") {
            dimension = "deploy"
        }
    }

    buildTypes {
        /* Enable or disable logging in build types */
        val SESSION_LOGGING = "SESSION_LOGGING"

        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")

            buildConfigField("boolean", SESSION_LOGGING, "false")
        }

        debug {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-DEBUG"
            isTestCoverageEnabled = true
            isDebuggable = true

            buildConfigField("boolean", SESSION_LOGGING, "true")
        }
    }

    splits {
        abi {
            isEnable = true
            reset()
            include("armeabi-v7a", "arm64-v8a", "x86", "x86_64")
            isUniversalApk = true
        }
    }

    val versionCodes = mapOf("armeabi-v7a" to 1, "arm64-v8a" to 2, "x86" to 3, "x86_64" to 4)
    applicationVariants.all {
        outputs.forEach { output ->
            val output = output as ApkVariantOutputImpl
            val key = output.getFilter(OutputFile.ABI)
            val abiCode = if (key == null) {
                0
            } else {
                versionCodes.getOrDefault(key, 0)
            }
            output.versionCodeOverride =
                abiCode * 1000000 + output.versionCode
        }
    }

    compileOptions {
        encoding = "UTF-8"
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    lint {
        abortOnError = false
    }

    buildFeatures {
        dataBinding = true
    }

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
        }
    }

    sourceSets {
        getByName("androidTest") {
            assets.srcDir(files("$projectDir/schemas"))
        }
    }
}

tasks.withType<Test> {
    testLogging {
        exceptionFormat = TestExceptionFormat.FULL
        events("started", "skipped", "passed", "failed")
        showStandardStreams = true
    }
}

val ROOM_VERSION = "2.5.0"
val LIFECYCLE_VERSION = "2.5.1"

dependencies {
    /* Testing */
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test:rules:1.5.0")
    androidTestImplementation("androidx.room:room-testing:$ROOM_VERSION")
    testAnnotationProcessor("com.google.auto.service:auto-service:1.0.1")

    /* AndroidX libraries */
    implementation("androidx.appcompat:appcompat:1.6.0")
    implementation("androidx.fragment:fragment:1.5.5")
    implementation("androidx.lifecycle:lifecycle-livedata-core:2.5.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel:2.5.1")
    implementation("androidx.multidex:multidex:2.0.1")
    implementation("androidx.paging:paging-runtime:3.1.1")
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.recyclerview:recyclerview-selection:1.1.0")
    implementation("androidx.room:room-rxjava2:$ROOM_VERSION")
    implementation("androidx.room:room-runtime:$ROOM_VERSION")
    kapt("androidx.room:room-compiler:$ROOM_VERSION") /* For schemas import */
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")
    implementation("androidx.work:work-runtime:2.7.1")
    implementation("com.google.android.material:material:1.7.0")
    implementation("androidx.viewpager2:viewpager2:1.0.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel:$LIFECYCLE_VERSION")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$LIFECYCLE_VERSION")
    implementation("androidx.preference:preference:1.2.0")
    // Fix for WorkManager https://github.com/google/ExoPlayer/issues/7993
    implementation("com.google.guava:guava:31.1-jre")

    /* ReactiveX */
    implementation("io.reactivex.rxjava2:rxjava:2.2.21")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")

    /* ACRA */
    val ACRA_VERSION = "5.9.7"
    implementation("ch.acra:acra-dialog:$ACRA_VERSION")
    implementation("ch.acra:acra-mail:$ACRA_VERSION")

    /* libtorrent wrapper */
    val LIBTORRENT4J_VERSION = "2.1.0-27"
    implementation("org.libtorrent4j:libtorrent4j:$LIBTORRENT4J_VERSION")
    implementation("org.libtorrent4j:libtorrent4j-android-arm:$LIBTORRENT4J_VERSION")
    implementation("org.libtorrent4j:libtorrent4j-android-arm64:$LIBTORRENT4J_VERSION")
    implementation("org.libtorrent4j:libtorrent4j-android-x86:$LIBTORRENT4J_VERSION")
    implementation("org.libtorrent4j:libtorrent4j-android-x86_64:$LIBTORRENT4J_VERSION")

    /* Other */
    implementation("com.github.cachapa:ExpandableLayout:2.9.2")
    implementation("com.h6ah4i.android.widget.advrecyclerview:advrecyclerview:1.0.0")
    implementation("com.github.anthonynsimon:jurl:v0.4.2")
    implementation("com.jaredrummler:colorpicker:1.1.0")
    /* Don"t use Commons IO >= 2.6, because they uses Java NIO, supported only from API 26 */
    //noinspection GradleDependency
    implementation("commons-io:commons-io:2.5")
    implementation("org.apache.commons:commons-text:1.10.0")
    implementation("com.google.code.gson:gson:2.10.1")
    implementation("com.leinardi.android:speed-dial:3.3.0")
    implementation("net.java.dev.jna:jna:5.13.0@aar")
}
