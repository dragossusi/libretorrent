/*
 * Copyright (C) 2019-2021 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.core.filter

import org.proninyaroslav.libretorrent.core.model.data.TorrentInfo
import org.proninyaroslav.libretorrent.core.model.data.TorrentStateCode
import org.proninyaroslav.libretorrent.core.model.data.entity.TagInfo
import org.proninyaroslav.libretorrent.core.utils.DateUtils

object TorrentFilterCollection {
    @JvmStatic
    fun all(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo? -> true }
    }

    @JvmStatic
    fun statusDownloading(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo -> state.stateCode == TorrentStateCode.DOWNLOADING }
    }

    @JvmStatic
    fun statusDownloaded(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo -> state.stateCode == TorrentStateCode.SEEDING || state.receivedBytes == state.totalBytes }
    }

    @JvmStatic
    fun statusDownloadingMetadata(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo -> state.stateCode == TorrentStateCode.DOWNLOADING_METADATA }
    }

    @JvmStatic
    fun statusError(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo -> state.error != null }
    }

    @JvmStatic
    fun dateAddedToday(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo ->
            val dateAdded = state.dateAdded
            val timeMillis = System.currentTimeMillis()
            dateAdded >= DateUtils.startOfToday(timeMillis) &&
                    dateAdded <= DateUtils.endOfToday(timeMillis)
        }
    }

    @JvmStatic
    fun dateAddedYesterday(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo ->
            val dateAdded = state.dateAdded
            val timeMillis = System.currentTimeMillis()
            dateAdded >= DateUtils.startOfYesterday(timeMillis) &&
                    dateAdded <= DateUtils.endOfYesterday(timeMillis)
        }
    }

    @JvmStatic
    fun dateAddedWeek(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo ->
            val dateAdded = state.dateAdded
            val timeMillis = System.currentTimeMillis()
            dateAdded >= DateUtils.startOfWeek(timeMillis) &&
                    dateAdded <= DateUtils.endOfWeek(timeMillis)
        }
    }

    @JvmStatic
    fun dateAddedMonth(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo ->
            val dateAdded = state.dateAdded
            val timeMillis = System.currentTimeMillis()
            dateAdded >= DateUtils.startOfMonth(timeMillis) &&
                    dateAdded <= DateUtils.endOfMonth(timeMillis)
        }
    }

    @JvmStatic
    fun dateAddedYear(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo ->
            val dateAdded = state.dateAdded
            val timeMillis = System.currentTimeMillis()
            dateAdded >= DateUtils.startOfYear(timeMillis) &&
                    dateAdded <= DateUtils.endOfYear(timeMillis)
        }
    }

    @JvmStatic
    fun tag(tag: TagInfo?): TorrentFilter {
        return TorrentFilter { state: TorrentInfo -> tag == null || state.tags.contains(tag) }
    }

    @JvmStatic
    fun noTags(): TorrentFilter {
        return TorrentFilter { state: TorrentInfo -> state.tags.isEmpty() }
    }
}
