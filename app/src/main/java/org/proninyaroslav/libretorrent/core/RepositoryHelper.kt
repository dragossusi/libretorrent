/*
 * Copyright (C) 2019-2021 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.core

import android.content.Context
import org.proninyaroslav.libretorrent.core.settings.SettingsRepository
import org.proninyaroslav.libretorrent.core.settings.SettingsRepositoryImpl
import org.proninyaroslav.libretorrent.core.storage.*

object RepositoryHelper {
    private var feedRepo: FeedRepositoryImpl? = null
    private var torrentRepo: TorrentRepositoryImpl? = null
    private var settingsRepo: SettingsRepositoryImpl? = null
    private var tagRepo: TagRepository? = null

    @JvmStatic
    @Synchronized
    fun getTorrentRepository(appContext: Context): TorrentRepository? {
        if (torrentRepo == null) torrentRepo = TorrentRepositoryImpl(
            appContext,
            AppDatabase.getInstance(appContext)
        )

        return torrentRepo
    }

    @JvmStatic
    @Synchronized
    fun getFeedRepository(appContext: Context): FeedRepository? {
        if (feedRepo == null) feedRepo = FeedRepositoryImpl(
            appContext,
            AppDatabase.getInstance(appContext)
        )

        return feedRepo
    }

    @JvmStatic
    @Synchronized
    fun getSettingsRepository(appContext: Context): SettingsRepository? {
        if (settingsRepo == null) settingsRepo = SettingsRepositoryImpl(appContext)

        return settingsRepo
    }

    @JvmStatic
    @Synchronized
    fun getTagRepository(appContext: Context): TagRepository? {
        if (tagRepo == null) tagRepo = TagRepositoryImpl(AppDatabase.getInstance(appContext))

        return tagRepo
    }
}
