/*
 * Copyright (C) 2019-2022 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.core.settings

import io.reactivex.Flowable

interface SettingsRepository {
    /*
     * Returns Flowable with key
     */
    fun observeSettingsChanged(): Flowable<String?>?

    fun readSessionSettings(): SessionSettings?

    /*
     * Appearance settings
     */
    fun notifySound(): String?

    fun notifySound(`val`: String?)

    fun torrentFinishNotify(): Boolean

    fun torrentFinishNotify(`val`: Boolean)

    fun playSoundNotify(): Boolean

    fun playSoundNotify(`val`: Boolean)

    fun ledIndicatorNotify(): Boolean

    fun ledIndicatorNotify(`val`: Boolean)

    fun vibrationNotify(): Boolean

    fun vibrationNotify(`val`: Boolean)

    fun theme(): Int

    fun theme(`val`: Int)

    fun ledIndicatorColorNotify(): Int

    fun ledIndicatorColorNotify(`val`: Int)

    fun foregroundNotifyStatusFilter(): String?

    fun foregroundNotifyStatusFilter(`val`: String?)

    fun foregroundNotifySorting(): String?

    fun foregroundNotifySorting(`val`: String?)

    fun foregroundNotifyCombinedPauseButton(`val`: Boolean)

    fun foregroundNotifyCombinedPauseButton(): Boolean

    /*
     * Behavior settings
     */
    fun autostart(): Boolean

    fun autostart(`val`: Boolean)

    fun keepAlive(): Boolean

    fun keepAlive(`val`: Boolean)

    fun shutdownDownloadsComplete(): Boolean

    fun shutdownDownloadsComplete(`val`: Boolean)

    fun cpuDoNotSleep(): Boolean

    fun cpuDoNotSleep(`val`: Boolean)

    fun onlyCharging(): Boolean

    fun onlyCharging(`val`: Boolean)

    fun batteryControl(): Boolean

    fun batteryControl(`val`: Boolean)

    fun customBatteryControl(): Boolean

    fun customBatteryControl(`val`: Boolean)

    fun customBatteryControlValue(): Int

    fun customBatteryControlValue(`val`: Int)

    fun unmeteredConnectionsOnly(): Boolean

    fun unmeteredConnectionsOnly(`val`: Boolean)

    fun enableRoaming(): Boolean

    fun enableRoaming(`val`: Boolean)

    /*
     * Network settings
     */
    fun portRangeFirst(): Int

    fun portRangeFirst(`val`: Int)

    fun portRangeSecond(): Int

    fun portRangeSecond(`val`: Int)

    fun enableDht(): Boolean

    fun enableDht(`val`: Boolean)

    fun enableLsd(): Boolean

    fun enableLsd(`val`: Boolean)

    fun enableUtp(): Boolean

    fun enableUtp(`val`: Boolean)

    fun enableUpnp(): Boolean

    fun enableUpnp(`val`: Boolean)

    fun enableNatPmp(): Boolean

    fun enableNatPmp(`val`: Boolean)

    fun useRandomPort(): Boolean

    fun useRandomPort(`val`: Boolean)

    fun encryptInConnections(): Boolean

    fun encryptInConnections(`val`: Boolean)

    fun encryptOutConnections(): Boolean

    fun encryptOutConnections(`val`: Boolean)

    fun enableIpFiltering(): Boolean

    fun enableIpFiltering(`val`: Boolean)

    fun ipFilteringFile(): String?

    fun ipFilteringFile(`val`: String?)

    fun encryptMode(): Int

    fun encryptMode(`val`: Int)

    fun showNatErrors(): Boolean

    fun showNatErrors(`val`: Boolean)

    fun anonymousMode(): Boolean

    fun anonymousMode(`val`: Boolean)

    fun seedingOutgoingConnections(): Boolean

    fun seedingOutgoingConnections(`val`: Boolean)

    fun defaultTrackersList(): String?

    fun defaultTrackersList(`val`: String?)

    fun validateHttpsTrackers(): Boolean

    fun validateHttpsTrackers(`val`: Boolean)

    /*
     * Storage settings
     */
    fun saveTorrentsIn(): String?

    fun saveTorrentsIn(`val`: String?)

    fun moveAfterDownload(): Boolean

    fun moveAfterDownload(`val`: Boolean)

    fun moveAfterDownloadIn(): String?

    fun moveAfterDownloadIn(`val`: String?)

    fun saveTorrentFiles(): Boolean

    fun saveTorrentFiles(`val`: Boolean)

    fun saveTorrentFilesIn(): String?

    fun saveTorrentFilesIn(`val`: String?)

    fun watchDir(): Boolean

    fun watchDir(`val`: Boolean)

    fun dirToWatch(): String?

    fun dirToWatch(`val`: String?)

    fun watchDirDeleteFile(): Boolean

    fun watchDirDeleteFile(`val`: Boolean)

    fun posixDiskIo(): Boolean

    fun posixDiskIo(`val`: Boolean)

    /*
     * Limitations settings
     */
    fun maxDownloadSpeedLimit(): Int

    fun maxDownloadSpeedLimit(`val`: Int)

    fun maxUploadSpeedLimit(): Int

    fun maxUploadSpeedLimit(`val`: Int)

    fun maxConnections(): Int

    fun maxConnections(`val`: Int)

    fun maxConnectionsPerTorrent(): Int

    fun maxConnectionsPerTorrent(`val`: Int)

    fun maxUploadsPerTorrent(): Int

    fun maxUploadsPerTorrent(`val`: Int)

    fun maxActiveUploads(): Int

    fun maxActiveUploads(`val`: Int)

    fun maxActiveDownloads(): Int

    fun maxActiveDownloads(`val`: Int)

    fun maxActiveTorrents(): Int

    fun maxActiveTorrents(`val`: Int)

    fun autoManage(): Boolean

    fun autoManage(`val`: Boolean)

    /*
     * Proxy settings
     */
    fun proxyType(): Int

    fun proxyType(`val`: Int)

    fun proxyAddress(): String?

    fun proxyAddress(`val`: String?)

    fun proxyPort(): Int

    fun proxyPort(`val`: Int)

    fun proxyPeersToo(): Boolean

    fun proxyPeersToo(`val`: Boolean)

    fun proxyRequiresAuth(): Boolean

    fun proxyRequiresAuth(`val`: Boolean)

    fun proxyLogin(): String?

    fun proxyLogin(`val`: String?)

    fun proxyPassword(): String?

    fun proxyPassword(`val`: String?)

    fun applyProxy(): Boolean

    fun applyProxy(`val`: Boolean)

    /*
     * Scheduling settings
     */
    fun enableSchedulingStart(): Boolean

    fun enableSchedulingStart(`val`: Boolean)

    fun enableSchedulingShutdown(): Boolean

    fun enableSchedulingShutdown(`val`: Boolean)

    fun schedulingStartTime(): Int

    fun schedulingStartTime(`val`: Int)

    fun schedulingShutdownTime(): Int

    fun schedulingShutdownTime(`val`: Int)

    fun schedulingRunOnlyOnce(): Boolean

    fun schedulingRunOnlyOnce(`val`: Boolean)

    fun schedulingSwitchWiFi(): Boolean

    fun schedulingSwitchWiFi(`val`: Boolean)

    /*
     * Feed settings
     */
    fun feedItemKeepTime(): Long

    fun feedItemKeepTime(`val`: Long)

    fun autoRefreshFeeds(): Boolean

    fun autoRefreshFeeds(`val`: Boolean)

    fun refreshFeedsInterval(): Long

    fun refreshFeedsInterval(`val`: Long)

    fun autoRefreshFeedsUnmeteredConnectionsOnly(): Boolean

    fun autoRefreshFeedsUnmeteredConnectionsOnly(`val`: Boolean)

    fun autoRefreshFeedsEnableRoaming(): Boolean

    fun autoRefreshFeedsEnableRoaming(`val`: Boolean)

    fun feedStartTorrents(): Boolean

    fun feedStartTorrents(`val`: Boolean)

    fun feedRemoveDuplicates(): Boolean

    fun feedRemoveDuplicates(`val`: Boolean)

    /*
     * Streaming settings
     */
    fun enableStreaming(): Boolean

    fun enableStreaming(`val`: Boolean)

    fun streamingHostname(): String?

    fun streamingHostname(`val`: String?)

    fun streamingPort(): Int

    fun streamingPort(`val`: Int)

    /*
     * Logging settings
     */
    fun logging(): Boolean

    fun logging(`val`: Boolean)

    fun maxLogSize(): Int

    fun maxLogSize(`val`: Int)

    fun logSessionFilter(): Boolean

    fun logSessionFilter(`val`: Boolean)

    fun logDhtFilter(): Boolean

    fun logDhtFilter(`val`: Boolean)

    fun logPeerFilter(): Boolean

    fun logPeerFilter(`val`: Boolean)

    fun logPortmapFilter(): Boolean

    fun logPortmapFilter(`val`: Boolean)

    fun logTorrentFilter(): Boolean

    fun logTorrentFilter(`val`: Boolean)

    fun askManageAllFilesPermission(): Boolean

    fun askManageAllFilesPermission(`val`: Boolean)

    fun showManageAllFilesWarningDialog(): Boolean

    fun showManageAllFilesWarningDialog(`val`: Boolean)

    fun askNotificationPermission(): Boolean

    fun askNotificationPermission(`val`: Boolean)
}
