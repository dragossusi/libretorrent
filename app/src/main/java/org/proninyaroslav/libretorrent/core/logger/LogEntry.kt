/*
 * Copyright (C) 2020 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.core.logger

import java.text.SimpleDateFormat
import java.util.*

data class LogEntry(
    @JvmField val id: Int,
    @JvmField val tag: String,
    @JvmField val msg: String,
    val timeStamp: Long
) {
    private val timeStampFormatter: SimpleDateFormat = SimpleDateFormat(
        defaultTimeStampFormatter,
        Locale.getDefault()
    )

    val timeStampAsString: String
        get() = timeStampFormatter.format(this.timeStamp)

    fun toStringWithTimeStamp(): String {
        return timeStampAsString + " " + toString()
    }

    override fun toString(): String = "[$tag] $msg"

    companion object {
        private const val defaultTimeStampFormatter = "yyyy-MM-dd HH:mm:ss.SSS"
    }
}
