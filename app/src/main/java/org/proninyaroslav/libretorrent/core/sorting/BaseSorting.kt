/*
 * Copyright (C) 2016 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.core.sorting

open class BaseSorting(
    @JvmField val columnName: String,
    @JvmField val direction: Direction
) {
    enum class Direction {
        ASC, DESC;

        companion object {
            fun fromValue(value: String): Direction {
                for (direction in Direction.entries) {
                    if (direction.toString().equals(value, ignoreCase = true)) {
                        return direction
                    }
                }

                return ASC
            }
        }
    }

    interface SortingColumnsInterface<F> {
        fun compare(item1: F, item2: F, direction: Direction?): Int

        fun name(): String?
    }

    override fun toString(): String {
        return "BaseSorting{" +
                "direction=" + direction +
                ", columnName='" + columnName + '\'' +
                '}'
    }
}
