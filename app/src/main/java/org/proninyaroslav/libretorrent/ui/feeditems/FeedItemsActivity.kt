/*
 * Copyright (C) 2018 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.feeditems

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import org.proninyaroslav.libretorrent.R
import org.proninyaroslav.libretorrent.core.utils.Utils
import org.proninyaroslav.libretorrent.ui.FragmentCallback
import org.proninyaroslav.libretorrent.ui.FragmentCallback.ResultCode

class FeedItemsActivity : AppCompatActivity(), FragmentCallback {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(Utils.getAppTheme(applicationContext))
        super.onCreate(savedInstanceState)

        if (Utils.isTwoPane(this)) {
            finish()
            return
        }

        setContentView(R.layout.activity_feed_items)

        val feedItemsFragment = supportFragmentManager
            .findFragmentById(R.id.feed_items_fragmentContainer) as FeedItemsFragment?

        if (feedItemsFragment != null) feedItemsFragment.feedId = intent.getLongExtra(TAG_FEED_ID, -1)
    }

    override fun onFragmentFinished(
        f: Fragment, intent: Intent,
        code: ResultCode
    ) {
        finish()
    }

    companion object {
        private val TAG: String = FeedItemsActivity::class.java.simpleName

        const val TAG_FEED_ID: String = "feed_id"
    }
}
