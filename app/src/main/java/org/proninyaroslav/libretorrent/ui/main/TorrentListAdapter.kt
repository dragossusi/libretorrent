/*
 * Copyright (C) 2016-2021 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.main

import android.text.format.DateUtils
import android.text.format.Formatter
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import org.proninyaroslav.libretorrent.R
import org.proninyaroslav.libretorrent.core.model.data.TorrentInfo
import org.proninyaroslav.libretorrent.core.model.data.TorrentStateCode
import org.proninyaroslav.libretorrent.core.utils.Utils
import org.proninyaroslav.libretorrent.databinding.ItemTorrentListBinding
import org.proninyaroslav.libretorrent.ui.Selectable
import java.util.concurrent.atomic.AtomicReference

class TorrentListAdapter(private val listener: ClickListener) :
    ListAdapter<TorrentListItem, TorrentListAdapter.ViewHolder>(
        diffCallback
    ), Selectable<TorrentListItem?> {
    private var selectionTracker: SelectionTracker<TorrentListItem>? = null
    private val currOpenTorrent = AtomicReference<TorrentListItem>()
    private var onBind = false

    fun setSelectionTracker(selectionTracker: SelectionTracker<TorrentListItem>?) {
        this.selectionTracker = selectionTracker
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemTorrentListBinding>(
            inflater,
            R.layout.item_torrent_list,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        onBind = true

        val item = getItem(position)

        selectionTracker?.let { holder.setSelected(it.isSelected(item)) }

        var isOpened = false
        val currTorrent = currOpenTorrent.get()
        if (currTorrent != null) isOpened = item!!.torrentId == currTorrent.torrentId

        holder.bind(item, isOpened, listener)

        onBind = false
    }

    override fun getItemKey(position: Int): TorrentListItem? {
        if (position < 0 || position >= currentList.size) return null

        return getItem(position)!!
    }

    override fun getItemPosition(key: TorrentListItem?): Int {
        return currentList.indexOf(key)
    }

    fun markAsOpen(item: TorrentListItem) {
        val prevItem = currOpenTorrent.getAndSet(item)

        if (onBind) return

        var position = getItemPosition(prevItem)
        if (position >= 0) notifyItemChanged(position)

        if (item != null) {
            position = getItemPosition(item)
            if (position >= 0) notifyItemChanged(position)
        }
    }

    val openedItem: TorrentListItem
        get() = currOpenTorrent.get()

    internal interface ViewHolderWithDetails {
        val itemDetails: ItemDetails
    }

    class ViewHolder internal constructor(private val binding: ItemTorrentListBinding) : RecyclerView.ViewHolder(
        binding.root
    ), ViewHolderWithDetails {
        private val playToPauseAnim: AnimatedVectorDrawableCompat?
        private val pauseToPlayAnim: AnimatedVectorDrawableCompat?
        private var currAnim: AnimatedVectorDrawableCompat? = null

        /* For selection support */
        private var selectionKey: TorrentListItem? = null
        private var isSelected = false

        init {
            Utils.colorizeProgressBar(itemView.context, binding.progress)
            playToPauseAnim = AnimatedVectorDrawableCompat.create(itemView.context, R.drawable.play_to_pause)
            pauseToPlayAnim = AnimatedVectorDrawableCompat.create(itemView.context, R.drawable.pause_to_play)
        }

        fun bind(item: TorrentListItem?, isOpened: Boolean, listener: ClickListener?) {
            val context = itemView.context
            selectionKey = item

            val a = context.obtainStyledAttributes(
                TypedValue().data, intArrayOf(
                    R.attr.selectableColor,
                    R.attr.defaultRectRipple
                )
            )
            var d = if (isSelected) a.getDrawable(0)
            else a.getDrawable(1)
            if (d != null) Utils.setBackground(itemView, d)
            a.recycle()

            itemView.setOnClickListener { v: View? ->
                /* Skip selecting and deselecting */
                if (isSelected) return@setOnClickListener
                listener?.onItemClicked(item!!)
            }

            setPauseButtonState(item!!.stateCode == TorrentStateCode.PAUSED)
            binding.pause.setOnClickListener { v: View? ->
                listener?.onItemPauseClicked(
                    item
                )
            }

            binding.name.text = item.name
            if (item.stateCode == TorrentStateCode.DOWNLOADING_METADATA) {
                binding.progress.isIndeterminate = true
            } else {
                binding.progress.isIndeterminate = false
                binding.progress.progress = item.progress
            }

            val statusString = when (item.stateCode) {
                TorrentStateCode.DOWNLOADING -> context.getString(R.string.torrent_status_downloading)
                TorrentStateCode.SEEDING -> context.getString(R.string.torrent_status_seeding)
                TorrentStateCode.PAUSED -> context.getString(R.string.torrent_status_paused)
                TorrentStateCode.STOPPED -> context.getString(R.string.torrent_status_stopped)
                TorrentStateCode.FINISHED -> context.getString(R.string.torrent_status_finished)
                TorrentStateCode.CHECKING -> context.getString(R.string.torrent_status_checking)
                TorrentStateCode.DOWNLOADING_METADATA ->
                    context.getString(R.string.torrent_status_downloading_metadata)

                TorrentStateCode.UNKNOWN,
                TorrentStateCode.ERROR,
                null -> ""
            }
            binding.status.text = statusString

            val totalBytes = Formatter.formatFileSize(context, item.totalBytes)
            val ETA = when {
                item.ETA >= TorrentInfo.MAX_ETA -> "\u2022 " + Utils.INFINITY_SYMBOL
                item.ETA == 0L -> ""
                else -> "\u2022 " + DateUtils.formatElapsedTime(item.ETA)
            }

            val receivedBytes =
                if (item.progress == 100) totalBytes
                else Formatter.formatFileSize(context, item.receivedBytes)

            binding.downloadCounter.text = context.getString(
                R.string.download_counter_ETA_template, receivedBytes, totalBytes,
                (if (item.totalBytes == 0L) 0 else item.progress), ETA
            )

            val downloadSpeed = Formatter.formatFileSize(context, item.downloadSpeed)
            val uploadSpeed = Formatter.formatFileSize(context, item.uploadSpeed)
            binding.downloadUploadSpeed.text =
                context.getString(R.string.download_upload_speed_template, downloadSpeed, uploadSpeed)

            binding.peers.text = context.getString(R.string.peers_template, item.peers, item.totalPeers)
            setPauseButtonState(item.stateCode == TorrentStateCode.PAUSED)

            if (item.error != null) {
                binding.error.visibility = View.VISIBLE
                binding.error.text = context.getString(R.string.error_template, item.error)
            } else {
                binding.error.visibility = View.GONE
            }

            d = if (isOpened) ContextCompat.getDrawable(context, R.color.primary_light)
            else ContextCompat.getDrawable(context, android.R.color.transparent)
            if (d != null) Utils.setBackground(binding.indicatorCurOpenTorrent, d)
        }

        fun setSelected(isSelected: Boolean) {
            this.isSelected = isSelected
        }

        override val itemDetails: ItemDetails
            get() = ItemDetails(selectionKey, bindingAdapterPosition)

        fun setPauseButtonState(isPause: Boolean) {
            binding.pause.isActivated = !isPause

            val prevAnim = currAnim
            currAnim = (if (isPause) pauseToPlayAnim else playToPauseAnim)
            binding.pause.setImageDrawable(currAnim)
            if (currAnim !== prevAnim) {
                currAnim!!.start()
            }
        }
    }

    interface ClickListener {
        fun onItemClicked(item: TorrentListItem)

        fun onItemPauseClicked(item: TorrentListItem)
    }

    /*
     * Selection support stuff
     */
    class KeyProvider internal constructor(private val selectable: Selectable<TorrentListItem>) :
        ItemKeyProvider<TorrentListItem?>(
            SCOPE_MAPPED
        ) {
        override fun getKey(position: Int): TorrentListItem? {
            return selectable.getItemKey(position)
        }

        override fun getPosition(key: TorrentListItem): Int {
            return selectable.getItemPosition(key)
        }
    }

    class ItemDetails internal constructor(
        private val selectionKey: TorrentListItem?,
        private val adapterPosition: Int
    ) : ItemDetailsLookup.ItemDetails<TorrentListItem>() {
        override fun getSelectionKey(): TorrentListItem? {
            return selectionKey
        }

        override fun getPosition(): Int {
            return adapterPosition
        }
    }

    class ItemLookup internal constructor(private val recyclerView: RecyclerView) :
        ItemDetailsLookup<TorrentListItem>() {
        override fun getItemDetails(e: MotionEvent): ItemDetails<TorrentListItem>? {
            val view = recyclerView.findChildViewUnder(e.x, e.y)
            if (view != null) {
                val viewHolder = recyclerView.getChildViewHolder(view)
                if (viewHolder is ViewHolder) return viewHolder.itemDetails
            }

            return null
        }
    }

    companion object {
        val diffCallback: DiffUtil.ItemCallback<TorrentListItem> =
            object : DiffUtil.ItemCallback<TorrentListItem>() {
                override fun areContentsTheSame(
                    oldItem: TorrentListItem,
                    newItem: TorrentListItem
                ): Boolean {
                    return oldItem.equalsContent(newItem)
                }

                override fun areItemsTheSame(
                    oldItem: TorrentListItem,
                    newItem: TorrentListItem
                ): Boolean {
                    return oldItem == newItem
                }
            }
    }
}
