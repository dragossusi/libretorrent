/*
 * Copyright (C) 2021 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import org.proninyaroslav.libretorrent.R

class PermissionDeniedDialog : BaseAlertDialog() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)

        val title = getString(R.string.perm_denied_title)
        val message =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) getString(R.string.perm_denied_warning_android_r) else getString(
                R.string.perm_denied_warning
            )
        val positiveText = getString(R.string.yes)
        val negativeText = getString(R.string.no)

        return buildDialog(
            title,
            message,
            null,
            positiveText,
            negativeText,
            null,
            false
        )
    }

    companion object {
        @JvmStatic
        fun newInstance(): PermissionDeniedDialog {
            val frag = PermissionDeniedDialog()

            val args = Bundle()
            frag.arguments = args

            return frag
        }
    }
}
