/*
 * Copyright (C) 2020 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.log

import androidx.paging.PositionalDataSource
import io.reactivex.disposables.Disposable
import org.proninyaroslav.libretorrent.core.logger.LogEntry
import org.proninyaroslav.libretorrent.core.logger.Logger

internal class LogDataSource(private val logger: Logger) : PositionalDataSource<LogEntry>() {
    private val disposable: Disposable

    init {
        disposable = logger.observeDataSetChanged()
            .subscribe { _ -> invalidate() }
    }

    override fun invalidate() {
        disposable.dispose()

        super.invalidate()
    }

    override fun loadInitial(
        params: LoadInitialParams,
        callback: LoadInitialCallback<LogEntry>
    ) {
        var paused = false
        if (!logger.isPaused) {
            logger.pause()
            paused = true
        }

        try {
            val entries: List<LogEntry>
            val numEntries = logger.numEntries
            var pos = params.requestedStartPosition

            if (params.requestedStartPosition < numEntries) {
                entries = logger.getEntries(params.requestedStartPosition, params.requestedLoadSize)
            } else if (params.requestedLoadSize <= numEntries) {
                pos = numEntries - params.requestedLoadSize
                entries = logger.getEntries(pos, params.requestedLoadSize)
            } else {
                pos = 0
                entries = logger.getEntries(pos, numEntries)
            }

            if (entries.isEmpty()) callback.onResult(entries, 0)
            else callback.onResult(entries, pos)
        } finally {
            if (paused) logger.resume()
        }
    }

    override fun loadRange(
        params: LoadRangeParams,
        callback: LoadRangeCallback<LogEntry>
    ) {
        var paused = false
        if (!logger.isPaused) {
            logger.pause()
            paused = true
        }

        try {
            val entries: List<LogEntry>
            val numEntries = logger.numEntries

            entries = if (params.startPosition < numEntries) logger.getEntries(params.startPosition, params.loadSize)
            else ArrayList(0)

            callback.onResult(entries)
        } finally {
            if (paused) logger.resume()
        }
    }
}
