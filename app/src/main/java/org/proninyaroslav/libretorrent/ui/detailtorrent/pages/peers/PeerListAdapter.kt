/*
 * Copyright (C) 2016, 2019 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.detailtorrent.pages.peers

import android.text.format.Formatter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.proninyaroslav.libretorrent.R
import org.proninyaroslav.libretorrent.core.model.data.PeerInfo
import org.proninyaroslav.libretorrent.core.utils.Utils
import org.proninyaroslav.libretorrent.databinding.ItemPeersListBinding
import org.proninyaroslav.libretorrent.ui.Selectable
import java.util.*

class PeerListAdapter(private val listener: ClickListener) :
    ListAdapter<PeerItem, PeerListAdapter.ViewHolder>(diffCallback), Selectable<PeerItem?> {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemPeersListBinding>(
            inflater,
            R.layout.item_peers_list,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    override fun submitList(list: List<PeerItem>?) {
        if (list != null) Collections.sort(list)

        super.submitList(list)
    }

    override fun getItemKey(position: Int): PeerItem? {
        if (position < 0 || position >= currentList.size) return null

        return getItem(position)
    }

    override fun getItemPosition(key: PeerItem?): Int {
        return currentList.indexOf(key)
    }

    interface ClickListener {
        fun onItemLongClick(item: PeerItem): Boolean
    }

    class ViewHolder(private val binding: ItemPeersListBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        init {
            Utils.colorizeProgressBar(itemView.context, binding.progress)
        }

        fun bind(item: PeerItem?, listener: ClickListener?) {
            val context = itemView.context

            itemView.setOnLongClickListener { v: View? ->
                if (listener == null) return@setOnLongClickListener false
                listener.onItemLongClick(item!!)
            }

            binding.ip.text = item!!.ip
            binding.progress.progress = item.progress

            binding.port.text = context.getString(R.string.peer_port, item.port)

            binding.relevance.text = context.getString(R.string.peer_relevance, item.relevance)

            var connectionType = ""
            when (item.connectionType) {
                PeerInfo.ConnectionType.BITTORRENT -> connectionType =
                    context.getString(R.string.peer_connection_type_bittorrent)

                PeerInfo.ConnectionType.WEB -> connectionType = context.getString(R.string.peer_connection_type_web)
                PeerInfo.ConnectionType.UTP -> connectionType = context.getString(R.string.peer_connection_type_utp)
            }
            binding.connectionType.text = context.getString(R.string.peer_connection_type, connectionType)

            val downSpeed = Formatter.formatFileSize(context, item.downSpeed.toLong())
            val upSpeed = Formatter.formatFileSize(context, item.upSpeed.toLong())
            binding.upDownSpeed.text = context.getString(R.string.download_upload_speed_template, downSpeed, upSpeed)

            binding.client.text = context.getString(R.string.peer_client, item.client)

            val upload = Formatter.formatFileSize(context, item.totalUpload)
            val download = Formatter.formatFileSize(context, item.totalDownload)
            binding.totalDownloadUpload.text = context.getString(R.string.peer_total_download_upload, download, upload)
        }
    }

    companion object {
        private val TAG: String = PeerListAdapter::class.java.simpleName

        private val diffCallback: DiffUtil.ItemCallback<PeerItem> =
            object : DiffUtil.ItemCallback<PeerItem>() {
                override fun areContentsTheSame(
                    oldItem: PeerItem,
                    newItem: PeerItem
                ): Boolean {
                    return oldItem.equalsContent(newItem)
                }

                override fun areItemsTheSame(
                    oldItem: PeerItem,
                    newItem: PeerItem
                ): Boolean {
                    return oldItem == newItem
                }
            }
    }
}
