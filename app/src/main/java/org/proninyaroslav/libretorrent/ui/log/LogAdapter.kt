/*
 * Copyright (C) 2020 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import org.proninyaroslav.libretorrent.R
import org.proninyaroslav.libretorrent.core.logger.LogEntry
import org.proninyaroslav.libretorrent.databinding.ItemLogListBinding

internal class LogAdapter(private val listener: ClickListener) : PagedListAdapter<LogEntry, LogAdapter.ViewHolder>(
    diffCallback
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemLogListBinding>(
            inflater,
            R.layout.item_log_list,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entry = getItem(position)
        if (entry != null) holder.bind(entry, listener)
    }

    interface ClickListener {
        fun onItemClicked(entry: LogEntry)
    }

    class ViewHolder internal constructor(private val binding: ItemLogListBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(entry: LogEntry, listener: ClickListener?) {
            itemView.setOnClickListener { v: View? ->
                listener?.onItemClicked(entry)
            }

            binding.tag.text = entry.tag
            binding.msg.text = entry.msg
            binding.timeStamp.text = entry.timeStampAsString
        }
    }

    companion object {
        private val diffCallback: DiffUtil.ItemCallback<LogEntry> = object : DiffUtil.ItemCallback<LogEntry>(
        ) {
            override fun areContentsTheSame(
                oldItem: LogEntry,
                newItem: LogEntry
            ): Boolean {
                return oldItem == newItem
            }

            override fun areItemsTheSame(
                oldItem: LogEntry,
                newItem: LogEntry
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
