/*
 * Copyright (C) 2020 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.log

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class LogMutableParams : BaseObservable() {
    private var logging = false
    private var logSessionFilter = false
    private var logDhtFilter = false
    private var logPeerFilter = false
    private var logPortmapFilter = false
    private var logTorrentFilter = false

    @Bindable
    fun isLogging(): Boolean {
        return logging
    }

    fun setLogging(logging: Boolean) {
        this.logging = logging
        notifyPropertyChanged(BR.logging)
    }

    @Bindable
    fun isLogSessionFilter(): Boolean {
        return logSessionFilter
    }

    fun setLogSessionFilter(logSessionFilter: Boolean) {
        this.logSessionFilter = logSessionFilter
        notifyPropertyChanged(BR.logSessionFilter)
    }

    @Bindable
    fun isLogDhtFilter(): Boolean {
        return logDhtFilter
    }

    fun setLogDhtFilter(logDhtFilter: Boolean) {
        this.logDhtFilter = logDhtFilter
        notifyPropertyChanged(BR.logDhtFilter)
    }

    @Bindable
    fun isLogPeerFilter(): Boolean {
        return logPeerFilter
    }

    fun setLogPeerFilter(logPeerFilter: Boolean) {
        this.logPeerFilter = logPeerFilter
        notifyPropertyChanged(BR.logPeerFilter)
    }

    @Bindable
    fun isLogPortmapFilter(): Boolean {
        return logPortmapFilter
    }

    fun setLogPortmapFilter(logPortmapFilter: Boolean) {
        this.logPortmapFilter = logPortmapFilter
        notifyPropertyChanged(BR.logPeerFilter)
    }

    @Bindable
    fun isLogTorrentFilter(): Boolean {
        return logTorrentFilter
    }

    fun setLogTorrentFilter(logTorrentFilter: Boolean) {
        this.logTorrentFilter = logTorrentFilter
        notifyPropertyChanged(BR.logTorrentFilter)
    }

    override fun toString(): String {
        return "LogMutableParams{" +
                "logging=" + logging +
                ", logSessionFilter=" + logSessionFilter +
                ", logDhtFilter=" + logDhtFilter +
                ", logPeerFilter=" + logPeerFilter +
                ", logPortmapFilter=" + logPortmapFilter +
                ", logTorrentFilter=" + logTorrentFilter +
                '}'
    }
}
