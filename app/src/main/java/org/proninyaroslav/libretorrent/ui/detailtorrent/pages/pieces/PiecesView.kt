/*
 * Copyright (C) 2016, 2019 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.detailtorrent.pages.pieces

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import org.proninyaroslav.libretorrent.R
import org.proninyaroslav.libretorrent.core.utils.Utils
import kotlin.math.ceil
import kotlin.math.max

/*
 * A widget for display parts map.
 */
class PiecesView : View {
    private var pieces: BooleanArray?
    private var cells = 0
    private var cellSize = 0
    private var borderSize = 0
    private var stepSize = 0
    private var cols = 0
    private var rows = 0
    private var margin = 0
    var empty: Paint = Paint()
    var complete: Paint = Paint()

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        create(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        create(context, attrs)
    }

    fun create(context: Context, attrs: AttributeSet?) {
        cellSize = Utils.dpToPx(getContext(), CELL_SIZE_DP)
        borderSize = Utils.dpToPx(getContext(), BORDER_SIZE_DP)
        stepSize = cellSize + borderSize

        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.PiecesView)
            val color = a.getColor(R.styleable.PiecesView_pieces_cellColor, 0)
            empty.color = color
            a.recycle()
        }

        val a = context.obtainStyledAttributes(TypedValue().data, intArrayOf(R.attr.colorSecondary))
        complete.color = a.getColor(0, 0)
        a.recycle()
    }

    fun setPieces(pieces: BooleanArray?) {
        if (pieces == null || this.pieces.contentEquals(pieces)) return

        val prevLength = (if (this.pieces != null) this.pieces!!.size else 0)
        cells = pieces.size
        this.pieces = pieces
        if (prevLength == pieces.size) invalidate()
        else requestLayout()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val width = MeasureSpec.getSize(widthMeasureSpec) - paddingLeft - paddingRight
        cols = width / stepSize
        /* We don't limit rows in the smaller side, thereby preventing cuts display cells */
        rows = ceil((cells.toFloat() / cols.toFloat()).toDouble()).toInt()
        margin = (width - cols * stepSize) / 2
        val height = rows * stepSize

        setMeasuredDimension(width, max(width.toDouble(), height.toDouble()).toInt())
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (pieces == null) return

        var position = 0
        for (r in 0 until rows) {
            var c = 0
            while (c < cols && position < pieces!!.size) {
                val paint = (if (pieces!![position]) complete else empty)
                val left = c * stepSize + borderSize + margin
                val right = left + stepSize - borderSize * 2
                val top = r * stepSize + borderSize
                val bottom = top + stepSize - borderSize * 2

                canvas.drawRect(
                    (left + borderSize).toFloat(), (top + borderSize).toFloat(),
                    (right + borderSize).toFloat(), (bottom + borderSize).toFloat(), paint
                )
                ++position
                c++
            }
        }
    }

    companion object {
        private const val CELL_SIZE_DP = 20f
        private const val BORDER_SIZE_DP = 1f
    }
}