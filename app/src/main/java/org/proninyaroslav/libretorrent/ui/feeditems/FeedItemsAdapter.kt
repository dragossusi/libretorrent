/*
 * Copyright (C) 2018, 2019 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui.feeditems

import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.proninyaroslav.libretorrent.R
import org.proninyaroslav.libretorrent.databinding.ItemFeedItemsListBinding
import org.proninyaroslav.libretorrent.ui.Selectable
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class FeedItemsAdapter(private val listener: ClickListener) :
    ListAdapter<FeedItemsListItem, FeedItemsAdapter.ViewHolder>(
        diffCallback
    ), Selectable<FeedItemsListItem?> {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemFeedItemsListBinding>(
            inflater,
            R.layout.item_feed_items_list,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }

    override fun submitList(list: List<FeedItemsListItem>?) {
        if (list != null) Collections.sort(list)

        super.submitList(list)
    }

    override fun getItemKey(position: Int): FeedItemsListItem? {
        if (position < 0 || position >= currentList.size) return null

        return getItem(position)
    }


    override fun getItemPosition(key: FeedItemsListItem?): Int {
        return currentList.indexOf(key)
    }

    interface ClickListener {
        fun onItemClicked(item: FeedItemsListItem)

        fun onItemMenuClicked(menuId: Int, item: FeedItemsListItem)
    }

    class ViewHolder(private val binding: ItemFeedItemsListBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bind(item: FeedItemsListItem?, listener: ClickListener?) {
            val context = itemView.context

            binding.menu.setOnClickListener { v: View ->
                val popup = PopupMenu(v.context, v)
                popup.inflate(R.menu.feed_item_popup)

                val menu = popup.menu
                val markAsRead = menu.findItem(R.id.mark_as_read_menu)
                val markAsUnread = menu.findItem(R.id.mark_as_unread_menu)
                markAsRead?.setVisible(!item!!.read)
                markAsUnread?.setVisible(item!!.read)

                popup.setOnMenuItemClickListener { menuItem: MenuItem ->
                    listener?.onItemMenuClicked(menuItem.itemId, item!!)
                    true
                }
                popup.show()
            }

            itemView.setOnClickListener { v: View? ->
                listener?.onItemClicked(
                    item!!
                )
            }
            val styleAttr = if (item!!.read) android.R.attr.textColorSecondary
            else android.R.attr.textColorPrimary
            val a = context.obtainStyledAttributes(TypedValue().data, intArrayOf(styleAttr))
            binding.title.setTextColor(a.getColor(0, 0))
            a.recycle()
            binding.title.setTextAppearance(if (item.read) R.style.normalText else R.style.boldText)
            binding.title.text = item.title

            binding.pubDate.text = SimpleDateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)
                .format(Date(item.pubDate))
        }
    }

    companion object {
        private val TAG: String = FeedItemsAdapter::class.java.simpleName

        private val diffCallback: DiffUtil.ItemCallback<FeedItemsListItem> =
            object : DiffUtil.ItemCallback<FeedItemsListItem>(
            ) {
                override fun areContentsTheSame(
                    oldItem: FeedItemsListItem,
                    newItem: FeedItemsListItem
                ): Boolean {
                    return oldItem.equalsContent(newItem)
                }

                override fun areItemsTheSame(
                    oldItem: FeedItemsListItem,
                    newItem: FeedItemsListItem
                ): Boolean {
                    return oldItem == newItem
                }
            }
    }
}
