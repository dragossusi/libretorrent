/*
 * Copyright (C) 2018, 2019 Yaroslav Pronin <proninyaroslav@mail.ru>
 *
 * This file is part of LibreTorrent.
 *
 * LibreTorrent is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LibreTorrent is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LibreTorrent.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.proninyaroslav.libretorrent.ui

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize

/*
 * A base item for the file list adapters.
 */
open class FileItem constructor(
    @JvmField var index: Int,
    @JvmField var name: String?,
    @JvmField var isFile: Boolean,
    @JvmField var size: Long
) : Comparable<FileItem>, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readLong()
    ) {
    }

    override fun equals(other: Any?): Boolean {
        if (other !is FileItem) return false

        if (other === this) return true

        return index == other.index &&
                (name == null || name == other.name) && isFile == other.isFile
    }

    override fun hashCode(): Int {
        val prime = 31
        var result = 1

        result = prime * result + (if ((name == null)) 0 else name.hashCode())
        if (isFile) result = prime * result + index

        return result
    }

    override fun compareTo(other: FileItem): Int {
        return name!!.compareTo(other.name!!)
    }

    override fun toString(): String {
        return "FileItem{" +
                "index=" + index +
                ", name='" + name + '\'' +
                ", isFile=" + isFile +
                ", size=" + size +
                '}'
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(index)
        parcel.writeString(name)
        parcel.writeByte(if (isFile) 1 else 0)
        parcel.writeLong(size)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FileItem> {
        override fun createFromParcel(parcel: Parcel): FileItem {
            return FileItem(parcel)
        }

        override fun newArray(size: Int): Array<FileItem?> {
            return arrayOfNulls(size)
        }
    }
}
